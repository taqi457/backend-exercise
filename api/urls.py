from django.conf.urls import url
from .views import suggestView

urlpatterns = [
    url(r'^suggest/(?P<q>.+)$', suggestView.as_view() ),
]