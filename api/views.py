from django.http import HttpResponse, JsonResponse
from django.views.generic import View
import enchant

class suggestView(View):
    def get(self, request, q):
        d = enchant.Dict("en_US")
        result = d.suggest(q)
        return JsonResponse({"suggestions" : result})
